package com.example.mailservice.service;

import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Content;
import com.sendgrid.helpers.mail.objects.Email;
import com.sendgrid.helpers.mail.objects.Personalization;
import org.springframework.stereotype.Service;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Properties;
import java.util.logging.Logger;

@Service
public class EmailService {

    public void sendMail(String toEmail, String subject, String message) throws MessagingException {
        /*Properties props = new Properties();
        Session session = Session.getInstance(props, null);
        try {
            Message msg = new MimeMessage(session);
            msg.setFrom(new InternetAddress("itee.logiweb@gmail.com", "itee-logiweb Admin"));
            msg.addRecipient(Message.RecipientType.TO,
                    new InternetAddress("jeremy276@gmail.com", "Mr. User"));
            msg.setSubject("Your Example.com account has been activated");
            msg.setText("This is a test");
            Transport.send(msg);
        } catch (AddressException e) {
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }*/
        Email from = new Email("itee.logiweb@gmail.com");
        String sb = "Sending with SendGrid is Fun";
        Email to = new Email("jeremy276@gmail.com");
        Content content = new Content("text/plain", "and easy to do anywhere, even with Java");
        Mail mail = new Mail();
        mail.setFrom(from);
        mail.setSubject(sb);
        mail.setTemplateId("d-a12f5d9443894c4cb574bb3724d840d5");


        Personalization personalization = new Personalization();
        personalization.addDynamicTemplateData("user_name", "Thomas Chekroun");
        personalization.addTo(to);

        mail.addPersonalization(personalization);
        SendGrid sg = new SendGrid("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");
        Request request = new Request();
        try {
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());
            Response response = sg.api(request);
            System.out.println(response.getStatusCode());
            System.out.println(response.getBody());
            System.out.println(response.getHeaders());
        } catch (IOException ex) {
            System.out.println(ex.getStackTrace());
        }
    }
}