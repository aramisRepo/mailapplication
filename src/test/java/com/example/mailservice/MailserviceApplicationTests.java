package com.example.mailservice;

import com.example.mailservice.service.EmailService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.mail.MessagingException;

@SpringBootTest
class MailserviceApplicationTests {

	@Test
	void contextLoads() {
	}

	@Autowired
	private EmailService emailService;

	@Test
	public void testEmail() throws MessagingException {
		emailService.sendMail("frank23@example.com", "Test subject", "Test mail");
	}

}
